Version 0.27:

- fixed a bug in sharing files that appeared on android api versions > 23
- imagepipe no longer supports android versions lower than 2.2