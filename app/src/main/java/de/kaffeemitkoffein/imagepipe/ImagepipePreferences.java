package de.kaffeemitkoffein.imagepipe;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class ImagepipePreferences {

    private static final String PREF_SCALEMAX = "PREF_scale_max_diameter";
    private static final String PREF_FILENAME = "PREF_filename";
    private static final String PREF_QUALITY = "PREF_quality";
    private static final String PREF_MAXQUALITY = "PREF_maxquality";
    private static final String PREF_AUTOPIPE = "PREF_autopipe";
    private static final String PREF_NUMBERING = "PREF_numbering";
    private static final String PREF_AUTOSCALE = "PREF_autoscale";
    private static final String PREF_AUTOROTATE = "PREF_autorotate";
    private static final String PREF_PREVIEWQUALITY = "PREF_previewquality";
    private static final String PREF_FORCEDOWNSCALE = "PREF_forcedownscale";
    private static final String PREF_BUBLLECOUNTER = "PREF_bubblecounter";

    private static final String PREF_SCALEMAX_DEFAULT = "1024";
    private static final String PREF_FILENAME_DEFAULT = "Imagepipe";
    private static final int PREF_QUALITY_DEFAULT = 70;
    private static final String PREF_MAXQUALITY_DEFAULT = "80";
    private static final boolean PREF_AUTOPIPE_DEFAULT = true;
    private static final String PREF_NUMBERING_DEFAULT = "1";
    private static final boolean PREF_AUTOSCALE_DEFAULT = true;
    private static final boolean PREF_AUTOROTATE_DEFAULT = true;
    private static final boolean PREF_PREVIEWQUALITY_DEFAULT = true;
    private static final boolean PREF_FORCEDOWNSCALE_DEFAULT = true;
    private static final int PREF_BUBLLECOUNTER_DEFAULT = 0;

    public String scalemax = PREF_SCALEMAX_DEFAULT;
    public String filename = PREF_FILENAME_DEFAULT;
    public int quality = PREF_QUALITY_DEFAULT;
    public String quality_maxvalue = PREF_MAXQUALITY_DEFAULT;
    public boolean autopipe = PREF_AUTOPIPE_DEFAULT;
    public String numbering = PREF_NUMBERING_DEFAULT;
    public boolean autoscale = PREF_AUTOSCALE_DEFAULT;
    public boolean autorotate = PREF_AUTOROTATE_DEFAULT;
    public boolean previewquality = PREF_PREVIEWQUALITY_DEFAULT;
    public boolean forcedownscale = PREF_FORCEDOWNSCALE_DEFAULT;
    public int bubblecounter = PREF_BUBLLECOUNTER_DEFAULT;

    private Context context;
    SharedPreferences sharedPreferences;

    public ImagepipePreferences(Context c){
        this.context = c;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        readPreferences();
    }

    public void readPreferences(){
        this.scalemax = readPreference(PREF_SCALEMAX,PREF_SCALEMAX_DEFAULT);
        this.filename = readPreference(PREF_FILENAME,PREF_FILENAME_DEFAULT);
        this.quality = readPreference(PREF_QUALITY,PREF_QUALITY_DEFAULT);
        this.quality_maxvalue = readPreference(PREF_MAXQUALITY,PREF_MAXQUALITY_DEFAULT);
        this.autopipe = readPreference(PREF_AUTOPIPE,PREF_AUTOPIPE_DEFAULT);
        this.numbering = readPreference(PREF_NUMBERING,PREF_NUMBERING_DEFAULT);
        this.autoscale = readPreference(PREF_AUTOSCALE,PREF_AUTOSCALE_DEFAULT);
        this.autorotate = readPreference(PREF_AUTOROTATE,PREF_AUTOROTATE_DEFAULT);
        this.previewquality = readPreference(PREF_PREVIEWQUALITY,PREF_PREVIEWQUALITY_DEFAULT);
        this.forcedownscale = readPreference(PREF_FORCEDOWNSCALE,PREF_FORCEDOWNSCALE_DEFAULT);
        this.bubblecounter = readPreference(PREF_BUBLLECOUNTER,PREF_BUBLLECOUNTER_DEFAULT);
    }

    public void savePreferences(){
        applyPreference(PREF_SCALEMAX,this.scalemax);
        applyPreference(PREF_FILENAME,this.filename);
        applyPreference(PREF_QUALITY,this.quality);
        applyPreference(PREF_MAXQUALITY,this.quality_maxvalue);
        applyPreference(PREF_AUTOPIPE,this.autopipe);
        applyPreference(PREF_NUMBERING,this.numbering);
        applyPreference(PREF_AUTOSCALE,this.autoscale);
        applyPreference(PREF_AUTOROTATE,this.autorotate);
        applyPreference(PREF_PREVIEWQUALITY,this.previewquality);
        applyPreference(PREF_FORCEDOWNSCALE,this.forcedownscale);
        applyPreference(PREF_BUBLLECOUNTER,this.bubblecounter);
    }

    public String readPreference(String p, String d){
        return sharedPreferences.getString(p,d);
    }

    public Boolean readPreference(String p, Boolean d){
        return sharedPreferences.getBoolean(p,d);
    }

    public int readPreference(String p, int d){
        return sharedPreferences.getInt(p,d);
    }

    public void applyPreference(String pref, String value){
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(pref,value);
        pref_editor.apply();
    }

    public void applyPreference(String pref, Boolean value){
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(pref,value);
        pref_editor.apply();
    }

    public void applyPreference(String pref, int value){
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(pref,value);
        pref_editor.apply();
    }

    public int getQualitymaxvalue() {
        int i;
        try {
            i = Integer.valueOf(quality_maxvalue);
            return i;
        } catch (NumberFormatException e) {
            quality_maxvalue = PREF_MAXQUALITY_DEFAULT;
            applyPreference(PREF_MAXQUALITY,quality_maxvalue);
            return Integer.valueOf(PREF_MAXQUALITY_DEFAULT);
        }
    }

}
