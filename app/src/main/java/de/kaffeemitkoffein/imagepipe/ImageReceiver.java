/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.kaffeemitkoffein.imagepipe;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.graphics.*;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.os.*;
import android.app.Activity;
import android.database.Cursor;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.Manifest;
import android.content.pm.PackageManager;

/**
 * This class provides all the relevant functions and interaction for Imagepipe.
 * It extends Activity and gives compatibility down to API 1.
 */

public class ImageReceiver extends Activity{

    /**
     * ID string for debugging. Should not occur in logs in any official release.
     */

    private final String IMAGEPIPE_TAG = "IMAGEPIPE-DEBUG";

    /**
     * Constants for callbacks after permissions were granted.
     * The two functions need the read/write to SD storage permission:
     * - select image from gallery
     * - pipe the image
     * - save image to disk
     *
     * missing_permissions_task holds the call type that has to be made once
     * permissions are granted.
     * */

    private static final int PERMISSION_CALLBACK = 98;

    private static final int MISSING_PERMISSIONS_TASK_sendImageUriIntent = 1;
    private static final int MISSING_PERMISSIONS_TASK_pipeTheImage = 2;
    private static final int MISSING_PERMISSIONS_TASK_bulkpipe = 4;
    private static final int MISSING_PERMISSIONS_TASK_saveImage = 3;

    private int missing_permissions_task;

    /**
     * Callback constant to identify the select from gallery task.
     */

    private static final int SELECT_FROM_GALLERY_CALLBACK=99;

    /**
     * Imagepipe displays a "bubble" showing some additional information.
     * The constants define the behaviour:
     * - DELAY_FOR_BUBBLE_DISPLAYS: will be displayed after x app starts
     *   from the launcher
     * - MAX_BUBBLE_DISPLAYS: # of times the bubble will be displayed
     * - BUBBLE_RUN_DELAY: will display x milliseconds after launch
     */

    private static final int DELAY_FOR_BUBBLE_DISPLAYS = 2;
    private static final int MAX_BUBBLE_DISPLAYS = 3;
    private static final int BUBBLE_RUN_DELAY = 3500;


    /**
     * Saved instance-state constants.
     */

    private static final String SIS_CUT_LINE_TOP="SIS_CUT_LINE_TOP";
    private static final String SIS_CUT_LINE_BOTTOM="SIS_CUT_LINE_BOTTOM";
    private static final String SIS_CUT_LINE_LEFT="SIS_CUT_LINE_LEFT";
    private static final String SIS_CUT_LINE_RIGHT="SIS_CUT_LINE_RIGHT";
    private static final String SIS_SCALE_RATIO="SIS_SCALE_RATIO";
    private static final String SIS_IMAGE_NEEDS_TO_BE_SAVED ="SIS_IMAGE_NEEDS_TO_BE_SAVED";
    private static final String SIS_LAST_IMAGE_URI="SIS_LAST_IMAGE_URI";
    private static final String SIS_NO_IMAGE_LOADED="SIS_NO_IMAGE_LOADED";
    private static final String SIS_JPEG_FILESIZE="SIS_JPEG_FILESIZE";
    private static final String SIS_ORIGINAL_FILESIZE="SIS_ORIGINAL_FILESIZE";
    private static final String SIS_PIPING_ALREADY_LAUNCHED="SIS_PIPING_ALREADY_LAUNCHED";

    /**
     * Constants for calling the ImagePipeInfo class that displays info text.
     */

    /**
     * Error codes.
     */

    private static final int ERROR_NO_IMAGE_APP = 1;
    private static final int ERROR_NO_BULK_IMAGE_APP = 2;

    private static final String DATA_TITLE="DATA_TITLE";
    private static final String DATA_TEXTRESOURCE="DATA_TEXTRESOURCE";
    private static final String DATA_BUTTONTEXT="DATA_BUTTONTEXT";

    /**
     * Debug string that is displayed above the image info in runtime.
     */

    private String debug = "";

    /**
     * This is the about... dialog. This needs to be defined here to prevent memory
     * leaks when the device is rotated.
     */

    private Dialog aboutDialog;
    private Dialog exifDialog;

    /**
     * Static filename for file in cache. This file is used to save a stream
     * into a file to get exif data.
     */

    private static final String TEMP_FILENAME="tempimage.jpg";

    /**
     * Variables that say if permissons are available.
     */

    private Boolean hasReadStoragePermission = false;
    private Boolean hasWriteStoragePermission = false;

    /**
     * The key bitmaps the program works with. Concept is:
     * Bitmap image : the visible bitmap with quality preview
     * Bitmap scaled: non-visible backup of the scaled bitmap, but without quality loss. This is
     *                used to calculate "image".
     * Bitmap original: this is a backup of the loaded image. It is "autoscaled" and
     *                  "autorotated".
     */

    private Bitmap image = null;
    private Bitmap scaled = null;
    private Bitmap original = null;

    /**
     * Temporary holds the received intent. This needs to be defined global
     * because it is used in async task.
     */

    private Intent call_intent;

    /**
     * Variable that holds the (main) imageview where the image is displayed.
     */

    private ImageView image_display;

    /**
     * The loaded savedInstanceState. Is populated in onCreate.
     */

    private Bundle savedInstanceState;

    /**
     * Local preferences instance.
     * */

    private ImagepipePreferences pref;

    /**
     * Is set true if info bubble is visible.
     */

    private Boolean bubble_visible = false;

    /**
     * Is true if no image was loaded.
     */

    private Boolean no_image_loaded = true;

    /**
     * Indicates if there is a gallery call pending. This is needed to catch
     * simultaneous gallery calls just after permissions were granted.
     */

    private Boolean gallery_already_called = false;

    /**
     * Floats holding the cut-line boundaries globally.
     *
     * "offset_"    : offset coordinates of a gesture to calculate frame changes.
     * scale_ratio  : scale factor of the bitmap inside the imageview (image_display).
     */

    private float cut_line_top;
    private float cut_line_bottom;
    private float cut_line_left;
    private float cut_line_right;
    private float scale_ratio;

    /**
     * x_pos and x_pos hold the last touch position of the 1st touch pointer.
     * x_pos and x_pos hold the last touch position of the 2st touch pointer.
     *
     * Both are used in the cropping feature. They are global to identify movement
     * directions easier.
     */

    float x_pos;
    float y_pos;
    float x1_pos;
    float y1_pos;

    /**
     * Constants for the cropping borders.
     */

    private final static int LEFT_CUTLINE = 0;
    private final static int RIGHT_CUTLINE = 1;
    private final static int TOP_CUTLINE = 2;
    private final static int BOTTOM_CUTLINE = 3;

    /**
     * Coordinates of the "+" button visible when no image is loaded. This is populated
     * on runtime.
     */

    private float x1_choose_button;
    private float x2_choose_button;
    private float y1_choose_button;
    private float y2_choose_button;

    /**
     * Indicates if there is a change that can be saved. Prevents multiple saving of the
     * same image into a file.
     */

    private boolean image_needs_to_be_saved = true;

    /**
     * Holds the uri of the latest saved file/image. Stored in safedinstancestate, but currently not used.
     */

    private ImageContainer lastImageContainer;

    /**
     *
     */

    private ArrayList<ImageContainer> multiplePipeList;

    /**
     * Global context constant for async tasks to refer the app context.
     */

    private Context context = this;

    /**
     * Filesize (preview) of the encoded jpeg file in bytes.
     * 0: no preview available / preview calculation failed.
     */

    private long jpeg_ecoded_filesize = 0;

    /**
     * Time in millis from when on the next jpeg preview is allowed to be calculated.
     * Uses millis since 1970. "0" is a failsave init that allows the next calculation
     * to be made immediately.
     *
     * On runtime, this is counted from time now + NEXT_UPDATE_TIME_DELAY
     */

    private long next_quality_update_time = 0;

    /**
     * Mandatory delay between jpeg calculations in RAM to safe resources.
     * See above.
     */

    private static final int NEXT_UPDATE_TIME_DELAY = 2000;

    private Boolean quality_update_is_dispatched = false;

    /**
     * Is true if there is a quality preview calculation already pending.
     * Prevents multiple calls.
     */
    private Boolean piping_was_already_launched = false;

    /**
     * Original filesize of the loaded image / stream.
     * Is used to calculate compression ratio.
     */

    private long original_image_filesize = 0;

    private ExifInterface exif_data;

    @Override
    protected void onCreate(Bundle sic) {
        super.onCreate(sic);
        savedInstanceState = sic;
        setContentView(R.layout.activity_image_receiver);
        imageEditor();
    }

    @Override
    protected void onResume() {
        super.onResume();
        readPreferences();
        check_for_displayImageQuality();
    }

    @Override
    protected void onPause() {
        if (aboutDialog != null){
            if (aboutDialog.isShowing())
                aboutDialog.dismiss();
        }
        if (exifDialog != null){
            if (exifDialog.isShowing())
                exifDialog.dismiss();
        }
        deleteCacheFile();
        super.onPause();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.image_receiver,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem mi){
       int item_id = mi.getItemId();
        if (item_id == R.id.menu_settings) {
            Intent i = new Intent(this, Settings.class);
            startActivity(i);
            return true;
        }
        if (item_id == R.id.menu_share) {
            sendImageUriIntent();
            return true;
        }
        if (item_id==R.id.menu_licence) {
            Intent i = new Intent(this, ImagePipeInfo.class);
            i.putExtra(DATA_TITLE, getResources().getString(R.string.license_title));
            i.putExtra(DATA_TEXTRESOURCE, "license");
            i.putExtra(DATA_BUTTONTEXT,getResources().getString(R.string.intro_button_text));
            startActivity(i);
            return true;
        }
        if (item_id == R.id.menu_exifdialog) {
            showExifDialog();
            return true;
        }
        if (item_id == R.id.menu_about) {
            showIntroDialog();
            return true;
        }
        return super.onOptionsItemSelected(mi);
    }

    /**
     * Draws the cut-frame inside the imageview. Coordinates are absolute values on the bitmap.
     * @param x1 x-coord
     * @param y1 y-coord
     * @param x2 x-coord
     * @param y2 y-coord
     * @param scale_ratio Scale ratio of the bitmap inside the imageview.
     */

    private void drawCutFrame(float x1,float y1, float x2, float y2, float scale_ratio){
        if (!no_image_loaded){
            Bitmap draw_bitmap;
           try {
                draw_bitmap = image.copy(image.getConfig(),true);
            } catch (OutOfMemoryError e){
                // nothing to do; this frame drawing may fail due to low memory.
                return;
            }
            Canvas canvas = new Canvas();
            canvas.setBitmap(draw_bitmap);
            Paint paint = new Paint();
            paint.setColor(getAppColor(R.color.secondaryLightColor));
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(5*scale_ratio);
            // canvas.drawCircle((x_touched-x_padding/2)*scale_ratio,(y_touched-y_padding/2)*scale_ratio,10,paint);
            canvas.drawRect(x1,y1,x2,y2,paint);
            if ((image_display.getWidth() !=0) && (image_display.getHeight() != 0)) {
                image_display.setImageBitmap(draw_bitmap);
                image_display.invalidate();
            }
            updateImagePropertiesText();
         }
    }

    /**
     * Draws the cut-frame inside the imageview. Coordinates are absolute values on the bitmap.
     * Debug version for also displaying touch-event coordinates inside the imageview. Not used in
     * the productive code.
     *
     * @param x1 x-coord
     * @param y1 y-coord
     * @param x2 x-coord
     * @param y2 y-coord
     * @param x3 x-coord of touch-event to display
     * @param y3 y-coord of touch-event to display
     * @param scale_ratio Scale ratio of the bitmap inside the imageview.
     */

    private void drawCutFrame(float x1,float y1, float x2, float y2, float scale_ratio, float x3, float y3){
        if (!no_image_loaded) {
            Bitmap draw_bitmap;
            try {
                draw_bitmap = image.copy(image.getConfig(),true);
            } catch (OutOfMemoryError e){
                // nothing to do; this frame drawing may fail due to low memory.
                return;
            }
            Canvas canvas = new Canvas();
            canvas.setBitmap(draw_bitmap);
            Paint paint = new Paint();
            paint.setColor(Color.CYAN);
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(5 * scale_ratio);
            canvas.drawCircle(x3, y3, 10, paint);
            canvas.drawRect(x1, y1, x2, y2, paint);
            if ((image_display.getWidth() !=0) && (image_display.getHeight() != 0)) {
                image_display.setImageBitmap(draw_bitmap);
                image_display.invalidate();
            }
        }
        }

    /**
     * Calculates the new cut-frame-borders from the touch-event.
     * All coordinates are BITMAP coordinates. They were corrected from the view-coordinates by taking the
     * scale ratio into consideration!
     * @param x_delta_image delta-x from the touch-event (swipe)
     * @param y_delta_image delta-y from the touch-event (swipe)
     * @param touch_is_inside_frame indicates if the event is inside the cut-frame (true) or outside (false)
     */

    private void calculateCutFramePosition(float x_delta_image, float y_delta_image, Boolean touch_is_inside_frame){
        if (touch_is_inside_frame) {
            if (x_delta_image < 0)
                cut_line_right = cut_line_right - Math.abs(x_delta_image);
            if (x_delta_image > 0)
                cut_line_left = cut_line_left + Math.abs(x_delta_image);
            if (y_delta_image < 0)
                cut_line_bottom = cut_line_bottom - Math.abs(y_delta_image);
            if (y_delta_image > 0)
                cut_line_top = cut_line_top + Math.abs(y_delta_image);
        } else {
            if (x_delta_image < 0)
                cut_line_left = cut_line_left - Math.abs(x_delta_image);
            if (x_delta_image > 0)
                cut_line_right = cut_line_right + Math.abs(x_delta_image);
            if (y_delta_image < 0)
                cut_line_top = cut_line_top - Math.abs(y_delta_image);
            if (y_delta_image > 0)
                cut_line_bottom = cut_line_bottom + Math.abs(y_delta_image);
        }
        if (cut_line_bottom<cut_line_top){
            float tmp = cut_line_bottom;
            cut_line_bottom = cut_line_top;
            cut_line_top = tmp;
        }
        if (cut_line_right<cut_line_left){
            float tmp = cut_line_right;
            cut_line_right = cut_line_left;
            cut_line_left = tmp;
        }
        fixCropOutOfBounds();
    }

    private void fixCropOutOfBounds(){
        if (cut_line_left<0)
            cut_line_left = 0;
        if (cut_line_right>image.getWidth())
            cut_line_right = image.getWidth();
        if (cut_line_top<0)
            cut_line_top = 0;
        if (cut_line_bottom>image.getHeight())
            cut_line_bottom = image.getHeight();
    }

    /**
     * Calculates the scale ratio of the bitmap inside the imageview.
     * @return float scale-ratio
     */

    private Float getScaleRatio(){
        float view_width = image_display.getWidth();
        float view_height = image_display.getHeight();
        float original_image_width = image.getWidth();
        float original_image_height = image.getHeight();
        float x_ratio = original_image_width / view_width;
        float y_ratio = original_image_height / view_height;
        if ( y_ratio > x_ratio ){
            return y_ratio;
        } else {
            return x_ratio;
        }
    }

    /**
     * Helper sub to get a color from resource.
     * @param color_resource is the R.id.color of the color.
     * @return int color value from a resource
     */

    @SuppressWarnings("deprecation")
    private int getAppColor(int color_resource){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            try {
                return getResources().getColor(color_resource, this.getTheme());
            } catch ( Exception e ) {
                return 0;
            }
            }
            else {
            try {
                return getResources().getColor(color_resource);
            } catch ( Exception e ) {
                return 0;
            }
            }
    }

    /**
     * A runnable that updates the image_display view with the image bitmap. This is only
     * a helper of the updateImageDisplay() call.
     */

    private void updateImageDisplay_runnable(){
        image_display.setImageBitmap(image);
        image_display.invalidate();
    }

    /**
     * Updates the image_display view with the image bitmap.
     * Checks if the view is already visible. If not, the call is queued.
     */

    private void updateImageDisplay(){
        if ((image_display.getWidth()==0) || (image_display.getHeight()==0)) {
            image_display.post(new Runnable() {
                @Override
                public void run() {
                    updateImageDisplay_runnable();
                }

            });
        } else
            updateImageDisplay_runnable();
    }

    private Boolean touchIsInsideFrame(float x_touched_image, float y_touched_image){
        if (    (x_touched_image > cut_line_left) &&
                (x_touched_image < cut_line_right) &&
                (y_touched_image > cut_line_top) &&
                (y_touched_image < cut_line_bottom)){
            return true;
        } else {
            return false;
        }
    }

    private int determineCutlinePointer(float x0,float y0,float x1,float y1, int pointer){
        if (pointer==LEFT_CUTLINE){
            if (x0<=x1) {
                return 0;
            } else {
                return 1;
            }
        }
        if (pointer==RIGHT_CUTLINE){
            if (x0>=x1) {
                return 0;
            } else {
                return 1;
            }
        }
        if (pointer==TOP_CUTLINE){
            if (y0<=y1) {
                return 0;
            } else {
                return 1;
            }
        }
        if (pointer==BOTTOM_CUTLINE)
            if (y0>=y1) {
                return 0;
            } else {
                return 1;
            }
        return 0;
    }

    private void calculateCutFrameMultitouch(View v, MotionEvent me, int historyindex){
        float x_padding = (v.getWidth()-image.getWidth()/scale_ratio)/2;
        float y_padding = (v.getHeight()-image.getHeight()/scale_ratio)/2;

        int i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),LEFT_CUTLINE);
        float x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        float y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        float x_delta = x_value - x_pos;
        float y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_left = cut_line_left + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),RIGHT_CUTLINE);
        x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_right = cut_line_right + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),TOP_CUTLINE);
        x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_top = cut_line_top + y_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),BOTTOM_CUTLINE);
        x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_bottom = cut_line_bottom + y_delta;

        fixCropOutOfBounds();
    }

    private void calculateCutFrameMultitouch(View v, MotionEvent me){
        float x_padding = (v.getWidth()-image.getWidth()/scale_ratio)/2;
        float y_padding = (v.getHeight()-image.getHeight()/scale_ratio)/2;

        int i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),LEFT_CUTLINE);
        float x_value = (me.getX(i) - x_padding) * scale_ratio;
        float y_value = (me.getY(i) - y_padding) * scale_ratio;
        float x_delta = x_value - x_pos;
        float y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_left = cut_line_left + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),RIGHT_CUTLINE);
        x_value = (me.getX(i) - x_padding) * scale_ratio;
        y_value = (me.getY(i) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_right = cut_line_right + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),TOP_CUTLINE);
        x_value = (me.getX(i) - x_padding) * scale_ratio;
        y_value = (me.getY(i) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_top = cut_line_top + y_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),BOTTOM_CUTLINE);
        x_value = (me.getX(i) - x_padding) * scale_ratio;
        y_value = (me.getY(i) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_bottom = cut_line_bottom + y_delta;

        fixCropOutOfBounds();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void imageEditor(){
        readPreferences();
        call_intent = getIntent();
        String i_action = call_intent.getAction();
        String i_type = call_intent.getType();
        image_display = (ImageView) findViewById(R.id.pipedimage);
        image_display.setOnTouchListener(new ImageView.OnTouchListener(){
        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
            public boolean onTouch(View v, MotionEvent me){
               if (bubble_visible)
                   updateImagePropertiesText();
               if (no_image_loaded){
                   if (!gallery_already_called)
                       if ((me.getX()>=x1_choose_button) && (me.getX()<=x2_choose_button) &&
                               (me.getY()>=y1_choose_button) && (me.getY()<=y2_choose_button)){
                       pickImageFromGallery();
                       gallery_already_called = true;
                       return true;
                        }
                   return false;
               }
               int action = me.getActionMasked();
               int pid    = me.getActionIndex();
                scale_ratio=getScaleRatio();
                float x_padding = (v.getWidth()-image.getWidth()/scale_ratio)/2;
                float y_padding = (v.getHeight()-image.getHeight()/scale_ratio)/2;
                for (int i=0; i<me.getPointerCount();i++) {
                    if ((me.getX(i) > x_padding) && (me.getX(i) < v.getWidth() - x_padding) &&
                            (me.getY(i) > y_padding) && (me.getY(i) < v.getHeight() - y_padding)) {
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                    }
                }
               if (action==MotionEvent.ACTION_DOWN){
                   x_pos = (me.getX() - x_padding) * scale_ratio;
                   y_pos = (me.getY() - y_padding) * scale_ratio;
                   drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                   return true;
               }
            if (action==MotionEvent.ACTION_POINTER_DOWN){
                x1_pos = (me.getX() - x_padding) * scale_ratio;
                y1_pos = (me.getY() - y_padding) * scale_ratio;
                drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                return true;
            }
               if (action==MotionEvent.ACTION_MOVE) {
                   if (me.getPointerCount() == 1) {
                       if (me.getHistorySize() > 0) {
                           x_pos = (me.getHistoricalX(0) - x_padding) * scale_ratio;
                           y_pos = (me.getHistoricalY(0) - y_padding) * scale_ratio;
                           int i = 0;
                           while (i < me.getHistorySize() - 2) {
                               i = i + 1;
                               float x_pos1 = (me.getHistoricalX(i) - x_padding) * scale_ratio;
                               float y_pos1 = (me.getHistoricalY(i) - y_padding) * scale_ratio;
                               float x_delta = x_pos1 - x_pos;
                               float y_delta = y_pos1 - y_pos;
                               calculateCutFramePosition(x_delta, y_delta, touchIsInsideFrame(x_pos, y_pos));
                               drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                               x_pos = x_pos1;
                               y_pos = y_pos1;
                           }
                       }
                       float x_pos1 = (me.getX() - x_padding) * scale_ratio;
                       float y_pos1 = (me.getY() - y_padding) * scale_ratio;
                       float x_delta = x_pos1 - x_pos;
                       float y_delta = y_pos1 - y_pos;
                       calculateCutFramePosition(x_delta, y_delta, touchIsInsideFrame(x_pos, y_pos));
                       drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                       x_pos = x_pos1;
                       y_pos = y_pos1;
                   } else {
                       if (me.getHistorySize() > 0) {
                           x_pos = (me.getHistoricalX(0,0) - x_padding) * scale_ratio;
                           y_pos = (me.getHistoricalY(0,0) - y_padding) * scale_ratio;
                           x1_pos = (me.getHistoricalX(1,0) - x_padding) * scale_ratio;
                           y1_pos = (me.getHistoricalY(1,0) - y_padding) * scale_ratio;
                           int i = 0;
                           while (i < me.getHistorySize() - 2) {
                               i = i + 1;
                               float x_pos1 = (me.getHistoricalX(0, i) - x_padding) * scale_ratio;
                               float y_pos1 = (me.getHistoricalY(0, i) - y_padding) * scale_ratio;
                               float x1_pos1 = (me.getHistoricalX(1, i) - x_padding) * scale_ratio;
                               float y1_pos1 = (me.getHistoricalY(1, i) - y_padding) * scale_ratio;
                               float x_delta = x_pos1 - x_pos;
                               float y_delta = y_pos1 - y_pos;
                               float x1_delta = x1_pos1 - x1_pos;
                               float y1_delta = y1_pos1 - y1_pos;
                               calculateCutFrameMultitouch(v, me, i);
                               drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                               x_pos = x_pos1;
                               y_pos = y_pos1;
                               x1_pos = x1_pos1;
                               y1_pos = y1_pos1;
                           }
                       }
                       float x_pos1 = (me.getX(0) - x_padding) * scale_ratio;
                       float y_pos1 = (me.getY(0) - y_padding) * scale_ratio;
                       float x1_pos1 = (me.getX(1) - x_padding) * scale_ratio;
                       float y1_pos1 = (me.getY(1) - y_padding) * scale_ratio;
                       calculateCutFrameMultitouch(v, me);
                       drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                       x_pos = x_pos1;
                       y_pos = y_pos1;
                       x1_pos = x1_pos1;
                       y1_pos = y1_pos1;
                   }
                   return true;
               }
            if (action==MotionEvent.ACTION_UP){
                x_pos = (me.getX() - x_padding) * scale_ratio;
                y_pos = (me.getY() - y_padding) * scale_ratio;
                float x_pos1 = (me.getX() - x_padding) * scale_ratio;
                float y_pos1 = (me.getY() - y_padding) * scale_ratio;
                float x_delta = x_pos1 - x_pos;
                float y_delta = y_pos1 - y_pos;
                calculateCutFramePosition(x_delta,y_delta,touchIsInsideFrame(x_pos,y_pos));
                drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                return true;
            }
            return false;
        }
        });

        Button button_rotateleft = (Button) findViewById(R.id.rotateleft_button);
        Button button_rotateright = (Button) findViewById(R.id.rotateright_button);
        Button button_processimage = (Button) findViewById(R.id.process_button);
        Button button_reloadimage = (Button) findViewById(R.id.reload_button);
        Button button_share = (Button) findViewById(R.id.share_button);
        Button button_save = (Button) findViewById(R.id.save_button);
        Button button_cut = (Button) findViewById(R.id.cut_button);
        Button button_gallery = (Button) findViewById(R.id.gallery_button);
        button_rotateleft.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rotateImage(-90)) {
                    float cut_line_bottom_new = image.getHeight() - cut_line_left;
                    float cut_line_top_new = image.getHeight() - cut_line_right;
                    float cut_line_left_new = cut_line_top;
                    float cut_line_right_new = cut_line_bottom;
                    cut_line_bottom = cut_line_bottom_new;
                    cut_line_top = cut_line_top_new;
                    cut_line_left = cut_line_left_new;
                    cut_line_right = cut_line_right_new;
                    updateImageDisplay();
                    drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                }
            }
        });
        button_rotateright.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rotateImage(90)) {
                    float cut_line_bottom_new = cut_line_right;
                    float cut_line_top_new = cut_line_left;
                    float cut_line_left_new = image.getWidth() - cut_line_bottom;
                    float cut_line_right_new = image.getWidth() - cut_line_top;
                    cut_line_bottom = cut_line_bottom_new;
                    cut_line_top = cut_line_top_new;
                    cut_line_left = cut_line_left_new;
                    cut_line_right = cut_line_right_new;
                    updateImageDisplay();
                    drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                }
            }
        });
        button_processimage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                scaleImage();
                updateImageDisplay();
                check_for_displayImageQuality();
            }
        });
        button_reloadimage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                reloadImage();
                updateImageDisplay();
            }
        });
        button_share.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                sendImageUriIntent();
            }
        });
        button_save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!hasReadWriteStorageRuntimePermission()) {
                    missing_permissions_task = MISSING_PERMISSIONS_TASK_saveImage;
                } else {
                    saveAndScanImage(true);
                }
            }
        });
        button_gallery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageFromGallery();
            }
        });
        button_cut.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!no_image_loaded){
                    if ((cut_line_left<cut_line_right) && (cut_line_top<cut_line_bottom)) {
                        try {
                            image = Bitmap.createBitmap(image, Math.round(cut_line_left), Math.round(cut_line_top), Math.round(cut_line_right - cut_line_left), Math.round(cut_line_bottom - cut_line_top));
                            if (scaled != null)
                                scaled = Bitmap.createBitmap(scaled, Math.round(cut_line_left), Math.round(cut_line_top), Math.round(cut_line_right - cut_line_left), Math.round(cut_line_bottom - cut_line_top));
                            updateImageDisplay();
                            image_needs_to_be_saved = true;
                            check_for_displayImageQuality();
                            updateImagePropertiesText();
                            resetCutFrame();
                        } catch (Exception e) {
                            // simply nothing to do if cut failed for some reason.
                        }
                    }
                }
            }
        });
        if (savedInstanceState != null) {
            image_needs_to_be_saved = savedInstanceState.getBoolean(SIS_IMAGE_NEEDS_TO_BE_SAVED, true);
            no_image_loaded = savedInstanceState.getBoolean(SIS_NO_IMAGE_LOADED, true);
            lastImageContainer = new ImageContainer();
            lastImageContainer.uri = savedInstanceState.getParcelable(SIS_LAST_IMAGE_URI);
            jpeg_ecoded_filesize = savedInstanceState.getLong(SIS_JPEG_FILESIZE);
            original_image_filesize = savedInstanceState.getLong(SIS_ORIGINAL_FILESIZE);
            if (lastImageContainer.uri != null) {
                readImageBitmapFromUri(lastImageContainer.uri);
                getSavedInstanceStateCutLine(savedInstanceState);
            }
            if (image == null)
                no_image_loaded = true;
            if (no_image_loaded) {
                setEmptyDefaultScreen();
                }
            check_for_displayImageQuality();
            updateImageDisplay();
            updateImagePropertiesText();
            } else {
            if ((Intent.ACTION_SEND.equals(i_action)) && (i_type != null)) {
                pipeTheImage(call_intent);
                } else {
                if ((Intent.ACTION_SEND_MULTIPLE.equals(i_action)) && (i_type != null)) {
                    // receive multiple images
                    pipeMultipleImages(call_intent);
                } else {
                    if (no_image_loaded) {
                        // catch no content received
                        setEmptyDefaultScreen();
                        check_for_BubbleHintDisplay();
                        }
                    }
                }
            }
        SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
        seekbar_quality.setMax(pref.getQualitymaxvalue());
        seekbar_quality.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                updateImagePropertiesText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // to do
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                writePreferences();
                check_for_displayImageQuality();
            }
        });

       EditText edit_size = (EditText) findViewById(R.id.editText_size);
        edit_size.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                writePreferences();
            }
        });
    }

    private void drawBubble(){
        int bubble_border = 20;
        int x = image_display.getWidth();
        int y = image_display.getHeight();
        int triangle_offset = x / 3;
        Bitmap bubblebitmap = Bitmap.createBitmap(x, y, Bitmap.Config.ARGB_8888);
        Canvas bubblecanvas = new Canvas();
        bubblecanvas.setBitmap(bubblebitmap);
        Path bubble = new Path();
        bubble.setFillType(Path.FillType.EVEN_ODD);
        bubble.moveTo(bubble_border,bubble_border);
        bubble.lineTo(x-bubble_border,bubble_border);
        bubble.lineTo(x-bubble_border,y-bubble_border*4);
        bubble.lineTo(triangle_offset+bubble_border,y-bubble_border*4);
        bubble.lineTo(triangle_offset,y);
        bubble.lineTo(triangle_offset-bubble_border,y-bubble_border*4);
        bubble.lineTo(bubble_border,y-bubble_border*4);
        bubble.lineTo(bubble_border,bubble_border);
        bubble.close();
        Paint paint = new Paint();
        paint.setColor(getAppColor(R.color.secondaryColor));
        paint.setAntiAlias(true);
        bubblecanvas.drawPath(bubble,paint);
        try {
            Bitmap icon_bitmap = BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher);
            bubblecanvas.drawBitmap(icon_bitmap,x-bubble_border*2-icon_bitmap.getWidth(),bubble_border*2,null);
        } catch (Exception e){
            // nothing to do; just failed loading the icon. This is not critical.
        }
        image_display.setImageBitmap(bubblebitmap);
        image_display.invalidate();
        TextView bubbletext= (TextView) findViewById(R.id.floating_text_over_image);
        bubbletext.setTextColor(getAppColor(R.color.secondaryTextColor));
        bubbletext.setGravity(Gravity.CENTER);
        String hint = getResources().getString(R.string.hint1_line1) +
                System.getProperty("line.separator") +
                System.getProperty("line.separator") +
                getResources().getString(R.string.hint1_line2) +
                System.getProperty("line.separator") +
                System.getProperty("line.separator") +
                getResources().getString(R.string.hint1_line3);
        bubbletext.setText(hint);
        bubble_visible = true;
    }

    private void check_for_BubbleHintDisplay(){
        if (pref.bubblecounter <= MAX_BUBBLE_DISPLAYS+DELAY_FOR_BUBBLE_DISPLAYS) {
            pref.bubblecounter = pref.bubblecounter + 1;
            pref.savePreferences();
            if (pref.bubblecounter > DELAY_FOR_BUBBLE_DISPLAYS) {
                Handler bubble_handler = new Handler();
                bubble_handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        drawBubble();
                    }
                }, BUBBLE_RUN_DELAY);
            }
        }
    }

    private int getExifRotationDataFromMediaStore(Uri image_uri){
        String [] imageColumns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.ORIENTATION};
        Cursor c = this.getContentResolver().query(image_uri,imageColumns,null,null,null);
        if (c == null)
            return -1;
        if (c.moveToFirst()){
            try {
                int i = c.getColumnIndex(imageColumns[1]);
                if (i != -1){
                    int degrees = c.getInt(i);
                    return degrees;
                } else {
                    // orientation-data does not exist.
                    return -1;
                }
            } catch (Exception e) {
                // some other data read error from media store
                return -1;
            }
        }
        c.close();
        return -1;
    }

    private int getExifRotationFromUri(Uri image_uri){
        // try to get rotation from system MediaStore
        int degrees = getExifRotationDataFromMediaStore(image_uri);
        if (degrees !=  -1) {
            return degrees;
        }
        // try to copy stream to file instead
        File tempfile = new File(this.getCacheDir(), TEMP_FILENAME);
        if (tempfile.exists()){
            if (tempfile.length()!=0){
                ExifInterface exif_data;
                try {
                    exif_data = new ExifInterface(tempfile.getAbsolutePath());
                } catch (Exception e){
                    // reading exif data from file failed, no rotation => returns 0
                    return 0;
                }
                switch (exif_data.getAttributeInt(ExifInterface.TAG_ORIENTATION,0)) {
                    case ExifInterface.ORIENTATION_ROTATE_270: degrees = 270;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180: degrees = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:  degrees = 90;
                        break;
                    default: degrees = 0;
                        break;
                }
                return degrees;
            }
            // delete temp file in cache if it exists.
            tempfile.delete();
        }
        // return 0 if the temporary file in cache does not exist.
        return 0;
    }

    private boolean deleteCacheFile(){
        File tempfile = new File(this.getCacheDir(), TEMP_FILENAME);
        return tempfile.delete();
    }

    @SuppressWarnings("deprecation")
    private void forceImageSizeDownscale() {
        Display d = getWindowManager().getDefaultDisplay();
        int height;
        int width;
        if (android.os.Build.VERSION.SDK_INT < 13) {
            width = d.getWidth();
            height = d.getHeight();
        } else {
            Point po = new Point();
            d.getSize(po);
            width = po.x;
            height = po.y;
        }
        int max_diameter_display;
        int max_diameter_image;
        if (width > height)
            max_diameter_display = width;
        else
            max_diameter_display = height;
        if (image.getWidth() > image.getHeight())
            max_diameter_image = image.getWidth();
        else
            max_diameter_image = image.getHeight();
        if (max_diameter_image > max_diameter_display)
            scaleImage(max_diameter_display);
    }

    private void applyChangesOnLoadedImage(Uri source_uri){
        readPreferences();
        next_quality_update_time = 0;
        if (!no_image_loaded){
            if (pref.forcedownscale){
                forceImageSizeDownscale();
            }
            if (pref.autoscale){
                scaleImage();
            }

            if (pref.autorotate) {
                rotateImage(getExifRotationFromUri(source_uri));
            }
            // delete temp image file in cache if it exists.
            deleteCacheFile();
            resetCutFrame();
            original = image;
            updateImageDisplay();
            check_for_displayImageQuality();
        }
    }

    private Boolean pipeTheImage(Intent intent){
        Uri source_uri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (!hasReadWriteStorageRuntimePermission()) {
            missing_permissions_task = MISSING_PERMISSIONS_TASK_pipeTheImage;
            setEmptyDefaultScreen();
            return false;
        }
        if ( source_uri != null ) {
            if (readImageBitmapFromUri(source_uri)) {
                applyChangesOnLoadedImage(source_uri);
                if ((pref.autopipe) && (!piping_was_already_launched)) {
                        piping_was_already_launched = true;
                        sendImageUriIntent();
                }
                } else {
                setEmptyDefaultScreen();
                }
        } else {
            setEmptyDefaultScreen();
        }
        return true;
    }

    /**
     * Extracts the uris from an arraylist of ImageContainers into an uri arraylist.
     * Returns null if the parameter is null or if the arraylist is empty.
     *
     * @param imageContainers
     * @return ArrayList<Uri>
     */

    private ArrayList<Uri> getUriArrayList(ArrayList<ImageContainer> imageContainers){
        if (imageContainers!=null) {
            if (imageContainers.size()>0) {
                ArrayList<Uri> urilist = new ArrayList<Uri>();
                for (int i=0; i<imageContainers.size(); i++){
                    if (imageContainers.get(i).uri != null){
                        urilist.add(imageContainers.get(i).uri);
                    } else {
                        // do nothing
                    }
                }
                return urilist;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private void processOneImageForMultiplePipe(final ArrayList<Uri> source_uris, final int i){
        if (i<source_uris.size()){
            if (readImageBitmapFromUri(source_uris.get(i))){
                final Context this_context = context;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        applyChangesOnLoadedImage(source_uris.get(i));
                        final ImageContainer imageContainer = saveImage(false);
                        if (imageContainer != null) {
                            if (imageContainer.file != null) {
                                FileMediaScanner fms = new FileMediaScanner(this_context,imageContainer.file,false){
                                    @Override
                                    public void onScanCompleted(String s, Uri uri){
                                        mediaScannerConnection.disconnect();
                                        imageContainer.uri = uri;
                                        imageContainer.file = new File(s);
                                        multiplePipeList.add(imageContainer);
                                        lastImageContainer = imageContainer;
                                        processOneImageForMultiplePipe(source_uris,i+1);
                                    }
                                };
                            }
                        }
                    }
                });
            }
        } else {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND_MULTIPLE);
            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM,getUriArrayList(multiplePipeList));
            intent.setType("image/jpeg");
            if (intent.resolveActivity(this.getPackageManager()) != null){
                try {
                    startActivity(Intent.createChooser(intent, getResources().getString(R.string.send_images_part1)+String.valueOf(i)+getResources().getString(R.string.send_images_part2)));
                } catch (ActivityNotFoundException e){
                    displayNoActivityFoundToast(ERROR_NO_BULK_IMAGE_APP);
                }
            } else {
                displayNoActivityFoundToast(ERROR_NO_BULK_IMAGE_APP);
            }
        }
    }

    private void displayNoActivityFoundToast(int error){
        switch (error) {
            case ERROR_NO_IMAGE_APP: Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.no_image_app_error),Toast.LENGTH_LONG).show(); break;
            case ERROR_NO_BULK_IMAGE_APP: Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.no_bulk_image_app_error),Toast.LENGTH_LONG).show(); break;
        }
    }

    private Boolean pipeMultipleImages(Intent intent){
        if (!hasReadWriteStorageRuntimePermission()) {
            missing_permissions_task = MISSING_PERMISSIONS_TASK_bulkpipe;
            setEmptyDefaultScreen();
            return false;
        }
        ArrayList<Uri> source_uris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (source_uris != null) {
            multiplePipeList = new ArrayList<ImageContainer>();
            processOneImageForMultiplePipe(source_uris,0);
        } else {
            setEmptyDefaultScreen();
        }
        return true;
    }

    private void reloadImage(){
        if (!no_image_loaded){
            image = original;
            if (scaled != null)
                scaled = original;
            check_for_displayImageQuality();
            updateImageDisplay();
            image_needs_to_be_saved = true;
            updateImagePropertiesText();
            resetCutFrame();
        }
    }

    private void scaleImage (int maxsize){
        if (!no_image_loaded){
            TextView text_size = (TextView) findViewById(R.id.editText_size);
            int target_width;
            int target_height;
            if (maxsize == 0){
                try {
                    maxsize = Integer.valueOf(text_size.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(this,R.string.toast_error_not_number,Toast.LENGTH_LONG).show();
                    maxsize = 1024;
                }
            }
            if (image.getHeight() > image.getWidth()){
                    target_height = maxsize;
                    target_width = Math.round(Float.valueOf(target_height) / Float.valueOf(image.getHeight()) * Float.valueOf(image.getWidth()));
            } else {
                    target_width = maxsize;
                    target_height= Math.round(Float.valueOf(target_width) / Float.valueOf(image.getWidth()) * Float.valueOf(image.getHeight()));
            }
            if (target_height<image.getHeight() || target_width<image.getWidth()) {
                if (scaled != null){
                    image = Bitmap.createScaledBitmap(scaled, target_width, target_height, false);
                } else {
                    image = Bitmap.createScaledBitmap(image, target_width, target_height, false);
                }
                if (scaled != null)
                    scaled = image;
                image_needs_to_be_saved = true;
                updateImagePropertiesText();
                }
            }
        }

    private void scaleImage (){
        scaleImage(0);
    }

    private Boolean rotateImage(float degree){
        if (degree == 0)
            return true;
        if (!no_image_loaded){
            Matrix matrix = new Matrix();
            matrix.postRotate(degree);
            try {
                image = Bitmap.createBitmap(image,0,0,image.getWidth(),image.getHeight(),matrix,true);
            } catch (OutOfMemoryError e){
                return false;
            }
            if (scaled != null)
                scaled = Bitmap.createBitmap(scaled,0,0,scaled.getWidth(),scaled.getHeight(),matrix,true);
            image_needs_to_be_saved = true;
            updateImagePropertiesText();
        }
        return true;
    }

    private class processJpegInBackground extends AsyncTask<Integer,Void,Boolean>{
        ByteArrayOutputStream out_stream;
        protected Boolean doInBackground(Integer... compression){
            if (image != null){
                try {
                    out_stream = new ByteArrayOutputStream();
                } catch (OutOfMemoryError e) {
                    return false;
                }
                if (scaled == null) {
                    try {
                        scaled = image;
                    } catch (OutOfMemoryError e) {
                        scaled = null;
                        return false;
                    }
                }
                try {
                    scaled.compress(Bitmap.CompressFormat.JPEG, compression[0], out_stream);
                } catch (OutOfMemoryError e){
                    scaled = null;
                    return false;
                }
                try {
                    out_stream.close();
                } catch (Exception e){
                    // nothing to do
                }
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean jpegresult) {
            super.onPostExecute(jpegresult);
            if (jpegresult){
                ByteArrayInputStream byis;
                try {
                     byis = new ByteArrayInputStream(out_stream.toByteArray());
                } catch (OutOfMemoryError e){
                    jpeg_ecoded_filesize = 0;
                    return;
                }
                try {
                    image = BitmapFactory.decodeStream(byis);
                } catch (OutOfMemoryError e){
                    jpeg_ecoded_filesize = 0;
                    return;
                }
                try {
                    byis.close();
                } catch (Exception e){
                    // nothing to do
                }
                jpeg_ecoded_filesize = out_stream.size();
                drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                next_quality_update_time = new Date().getTime() + NEXT_UPDATE_TIME_DELAY;
                updateImagePropertiesText();
            }
            quality_update_is_dispatched = false;
        }
    }

    private void displayImageQuality(){
        if (!no_image_loaded) {
            SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
            if (new Date().getTime() >= next_quality_update_time) {
                new processJpegInBackground().execute(seekbar_quality.getProgress());
            } else {
                if (!quality_update_is_dispatched) {
                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            displayImageQuality();
                        }
                    }, next_quality_update_time - new Date().getTime());
                    quality_update_is_dispatched = true;
                }
            }
        }
    }

    private boolean check_for_displayImageQuality(){
        if (!no_image_loaded){
            if (pref.previewquality){
                displayImageQuality();
                return true;
            } else {
                if (scaled != null) {
                    image = scaled;
                    scaled = null;
                    updateImageDisplay();
                }
                return false;
            }
        } else {
            return false;
        }
    }

    private String generateFileNameSkeleton(int i){
        String skeleton = pref.filename;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
       if (pref.numbering.equals("2")){
            return String.valueOf(i) + "_" + skeleton;
        }
        if (pref.numbering.equals("3")){
            return date.format(calendar.getTime()) + "_" + skeleton + "_" + String.valueOf(i);
        }
        if (pref.numbering.equals("4")){
            return String.valueOf(i) + "_" + skeleton + "_" + date.format(calendar.getTime());
        }
        return skeleton + "_" + String.valueOf(i);
    }

    private String determineUnusedFilename(File path){
        int i = 0;
        String fn;
        File check_file;
        do {
            fn = generateFileNameSkeleton(i) + ".jpg";
            i = i + 1;
            check_file = new File (path,fn);
        } while (check_file.exists());
        return fn;
    }

    private boolean hasReadWriteStorageRuntimePermission(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // handle runtime permissions only if android is >= Marshmellow api 23
            if (this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},PERMISSION_CALLBACK);
                // at this moment, we request the permission and leave with a negative result. The user needs to try saving again after permission was granted.
                return false;
            }
            else
            {
                // permission is granted, ok
                return true;
            }
        }
        else
        {
            // before api 23, permissions are always granted, so everything is ok
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int permRequestCode, String perms[], int[] grantRes){
        if (permRequestCode == PERMISSION_CALLBACK){
            int i;
            for (i=0; i<grantRes.length; i++){
                if (perms[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)){
                    if (grantRes[i]==PackageManager.PERMISSION_GRANTED){
                        hasReadStoragePermission=true;
                    } else {
                        hasReadStoragePermission=false;
                    }
                }
                if (perms[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    if (grantRes[i]==PackageManager.PERMISSION_GRANTED){
                        hasWriteStoragePermission=true;
                    } else {
                        hasWriteStoragePermission=false;
                    }
                }
            }
            if (hasReadStoragePermission && hasWriteStoragePermission) {
                if (missing_permissions_task == MISSING_PERMISSIONS_TASK_sendImageUriIntent) {
                    missing_permissions_task = 0;
                    sendImageUriIntent();
                }
                if (missing_permissions_task ==  MISSING_PERMISSIONS_TASK_pipeTheImage){
                    missing_permissions_task = 0;
                    pipeTheImage(call_intent);
                }
                if (missing_permissions_task ==  MISSING_PERMISSIONS_TASK_bulkpipe){
                    missing_permissions_task = 0;
                    pipeMultipleImages(call_intent);
                }
                if (missing_permissions_task ==  MISSING_PERMISSIONS_TASK_saveImage){
                    missing_permissions_task = 0;
                    saveAndScanImage(true);
                }
            } else {
                if (this.shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        this.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    showPermissionsRationale();
                }
            }

        }
    }

    private void showPermissionsRationale(){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialogpermrationale);
        dialog.setTitle(R.string.permissions_title);
        dialog.setCancelable(true);
        TextView tv = (TextView) dialog.findViewById(R.id.permrationale_text);
        tv.setText(getResources().getString(R.string.permissions_rationale_1)+
                    System.getProperty("line.separator")+
                getResources().getString(R.string.permissions_rationale_2)+
                System.getProperty("line.separator")+
                getResources().getString(R.string.permissions_rationale_3)+
                System.getProperty("line.separator")+
                getResources().getString(R.string.permissions_rationale_4)+
                System.getProperty("line.separator")+
                getResources().getString(R.string.permissions_rationale_5)+
                System.getProperty("line.separator")+
                getResources().getString(R.string.permissions_rationale_6)+
                System.getProperty("line.separator")+
                getResources().getString(R.string.permissions_rationale_7)+
                System.getProperty("line.separator")+
                getResources().getString(R.string.permissions_rationale_8)+
                System.getProperty("line.separator"));
        Button permok_button = (Button) dialog.findViewById(R.id.permok_button);
        permok_button.setOnClickListener(new OnClickListener(){
           @Override
           public void onClick(View v){
               dialog.dismiss();
           }
        });
        dialog.show();
    }

    private void sendImageUriIntent() {
        if (!hasReadWriteStorageRuntimePermission()) {
            missing_permissions_task = MISSING_PERMISSIONS_TASK_sendImageUriIntent;
            // break to exit sending here; it will be resumed through the permission callback if necessary
            return;
        }
        if (image_needs_to_be_saved) {
            ImageContainer imageContainer = saveImage(false);
            if (imageContainer != null){
                if (imageContainer.file != null){
                    new FileMediaScanner(getApplicationContext(),imageContainer.file,true);
                }
            }
        } else {
            if (lastImageContainer != null) {
                if (lastImageContainer.uri != null) {
                    shareImage(lastImageContainer);
                }
            }
        }
    }

    private ImageContainer saveImage(Boolean display_save_result){
        ImageContainer imageContainer = new ImageContainer();
        if (no_image_loaded){
            Toast.makeText(this, R.string.toast_load_image_first, Toast.LENGTH_LONG).show();
            return null;
        }
        if (image_needs_to_be_saved) {
            File system_picure_directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            String target_directory_string = system_picure_directory.getAbsolutePath()+File.separatorChar+getResources().getString(R.string.app_folder);
            File target_directory = new File(target_directory_string);
            target_directory.mkdirs();
            String targetname = determineUnusedFilename(target_directory);
            SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
            int compression_rate = seekbar_quality.getProgress();
            try {
                imageContainer.file = new File(target_directory, targetname);
                FileOutputStream fos = new FileOutputStream(imageContainer.file);
                if (scaled != null) {
                    scaled.compress(Bitmap.CompressFormat.JPEG, compression_rate, fos);
                } else {
                    image.compress(Bitmap.CompressFormat.JPEG, compression_rate, fos);
                }
                fos.flush();
                fos.close();
                lastImageContainer = null;
            } catch (Exception e) {
                Toast.makeText(this, R.string.toast_error_save_failed, Toast.LENGTH_LONG).show();
                return null;
            }
            image_needs_to_be_saved = false;
            if (display_save_result){
            Toast.makeText(this, getResources().getString(R.string.toast_save_successful),Toast.LENGTH_LONG).show();
        }
    } else {
            if (display_save_result) {
                Toast.makeText(this, getResources().getString(R.string.toast_save_not_necessary), Toast.LENGTH_LONG).show();
            }
            return lastImageContainer;
    }
    return imageContainer;
    }

    private void saveAndScanImage(Boolean display_save_result){
        ImageContainer imageContainer = saveImage(display_save_result);
        if (imageContainer != null){
            if (imageContainer.file != null){
                // scan image, update lastImageContainer and do not launch share
                FileMediaScanner fms = new FileMediaScanner(getApplicationContext(),imageContainer.file,false);
            }
        }
    }

    private void shareImage(ImageContainer imageContainer){
        Intent i = new Intent();
        i.setAction(Intent.ACTION_SEND);
        i.setDataAndType(imageContainer.uri, "image/jpeg");
        i.putExtra(Intent.EXTRA_STREAM, imageContainer.uri);
        i.setType("image/jpeg");
        if (i.resolveActivity(getPackageManager()) != null){
            try {
                startActivity(Intent.createChooser(i, getResources().getString(R.string.send_image)));
            } catch (ActivityNotFoundException e){
                displayNoActivityFoundToast(ERROR_NO_IMAGE_APP);
            }
        } else {
            displayNoActivityFoundToast(ERROR_NO_IMAGE_APP);
        }
    }

    private class FileMediaScanner implements MediaScannerConnection.MediaScannerConnectionClient{

        private File file;
        private Boolean share;
        public MediaScannerConnection mediaScannerConnection;

        public FileMediaScanner(Context c, File f, Boolean s){
            share = s;
            file = f;
            mediaScannerConnection = new MediaScannerConnection(context,this);
            mediaScannerConnection.connect();
        }

        @Override
        public void onMediaScannerConnected() {
           mediaScannerConnection.scanFile(file.getAbsolutePath(),null);
        }

        @Override
        public void onScanCompleted(String s, Uri uri) {
            mediaScannerConnection.disconnect();
            // lastImageContainer needs to be updated with the content-uri. Otherwise, sharing will fail on
            // API > 24.
            lastImageContainer = new ImageContainer();
            lastImageContainer.uri = uri;
            lastImageContainer.file = new File(s);
            if (share) {
                ImageContainer imageContainer = new ImageContainer();
                imageContainer.uri = uri;
                shareImage(imageContainer);
            }
        }
    }

    private String getKilobyteString(long l){
        l = l / 1000;
        return String.valueOf(l)+" kB";
    }

    private void updateImagePropertiesText(){
        if (!no_image_loaded){
            SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
            TextView infotext = (TextView) findViewById(R.id.imageproperties_text);
            String filesize ="";
            if ((pref.previewquality) && (jpeg_ecoded_filesize != 0)){
                // filesize = getResources().getString(R.string.size)+" "+getKilobyteString(jpeg_ecoded_filesize)+" of "+getKilobyteString(original_image_filesize);
                filesize = getResources().getString(R.string.size)+" "+getKilobyteString(jpeg_ecoded_filesize);
                if (original_image_filesize != 0){
                    filesize =  filesize + " ("+String.valueOf((jpeg_ecoded_filesize *100)/ original_image_filesize)+"%)";
                }
            } else filesize = "";
            String s = getResources().getString(R.string.width)+" "+String.valueOf(image.getWidth())+" "+getResources().getString(R.string.height)+" "+String.valueOf(image.getHeight())+" "+getResources().getString(R.string.quality)+" "+String.valueOf(seekbar_quality.getProgress()+" "+filesize+debug);
            infotext.setText(s);
        }
        if (bubble_visible){
            TextView bubbletext= (TextView) findViewById(R.id.floating_text_over_image);
            bubbletext.setText("");
            if (!no_image_loaded){
                updateImageDisplay();
            } else {
                setEmptyDefaultScreen();
            }
            bubble_visible = false;
        }
    }

    private void setEmptyDefaultScreen() {
        // View main_layout = (View) findViewById(R.layout.activity_image_receiver);
        View main_view = getWindow().getDecorView().getRootView();
        main_view.post(new Runnable(){
            @Override
            public void run(){
            Bitmap add_icon_blue = BitmapFactory.decodeResource(getResources(),R.mipmap.ic_choose_blue,null);
            int x = image_display.getWidth();
            int y = image_display.getHeight();
            Bitmap original = Bitmap.createBitmap(x, y, Bitmap.Config.ARGB_8888);
            Canvas no_image_canvas = new Canvas();
            no_image_canvas.setBitmap(original);
            x1_choose_button = (x-add_icon_blue.getWidth())/2;
            y1_choose_button = (y-add_icon_blue.getHeight())/2;
            x2_choose_button = (x+add_icon_blue.getWidth())/2;
            y2_choose_button = (y+add_icon_blue.getHeight())/2;
            no_image_canvas.drawBitmap(add_icon_blue,x1_choose_button,y1_choose_button,null);
            Paint paint = new Paint();
            paint.setColor(getAppColor(R.color.primaryTextColor));
            int text_size = y / 10;
            paint.setTextSize(text_size);
            float text_width = paint.measureText(getResources().getString(R.string.empty_imageview_text));
            while (paint.measureText(getResources().getString(R.string.empty_imageview_text))>x-x/20) {
                text_size = text_size - 1;
                paint.setTextSize(text_size);
            }
            no_image_canvas.drawText(getResources().getString(R.string.empty_imageview_text), x/40, (y-add_icon_blue.getHeight())/2 - text_size*2,paint);
        if (original != null) {
            image = original;
            if (scaled != null)
                scaled = original;
            updateImageDisplay();
            no_image_loaded = true;
            resetCutFrame();
        }}});
    }

    private void resetCutFrame(){
        cut_line_bottom = image.getHeight();
        cut_line_left = 0;
        cut_line_right = image.getWidth();
        cut_line_top = 0;
        getScaleRatio();
    }

    private void pickImageFromGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent,SELECT_FROM_GALLERY_CALLBACK);
    }

    private Boolean readImageBitmapFromUri(Uri source_uri){
        try {
            if (pref == null){
                readPreferences();
            }
            InputStream image_inputstream = new BufferedInputStream(this.getContentResolver().openInputStream(source_uri));
            ByteArrayOutputStream image_bytestream = new ByteArrayOutputStream();
            // if autorotate is enabled, file is temporarily saved in cache for further use to determine rotation later.
            File tempfile = new File(getApplicationContext().getCacheDir(),TEMP_FILENAME);
            FileOutputStream fileOutputStream = new FileOutputStream(tempfile);
            byte [] bytebuffer = new byte[1024*16];
            int i = 0;
            while ((i = image_inputstream.read(bytebuffer)) != -1){
                image_bytestream.write(bytebuffer,0,i);
                if (pref.autorotate){
                    // only write to cache if autorotate is enabled.
                    fileOutputStream.write(bytebuffer);
                }
            }
            image_inputstream.close();
            fileOutputStream.close();
            try {
                exif_data = new ExifInterface(tempfile.getAbsolutePath());
            } catch (Exception e){
                // ignore, if file name cannot be found.
            }
            original_image_filesize = image_bytestream.size();
            image = BitmapFactory.decodeStream(new ByteArrayInputStream(image_bytestream.toByteArray()));
            image_bytestream.close();
        }
        catch (Exception e){
            Toast.makeText(this, R.string.toast_error_image_not_loaded, Toast.LENGTH_LONG).show();
            no_image_loaded = true;
            return false;
        }
        scaled = null;
        no_image_loaded = false;
        image_needs_to_be_saved = true;
        return true;
    }

    @Override
    protected void onActivityResult(int rc, int rescode, Intent data){
        if ((rc==SELECT_FROM_GALLERY_CALLBACK) && (rescode==RESULT_OK)){
            call_intent = data;
            Uri i_uri = data.getData();
            if (i_uri != null) {
                image_display.setImageBitmap(null);
                image_display.invalidate();
                readImageBitmapFromUri(i_uri);
                gallery_already_called = false;
                applyChangesOnLoadedImage(i_uri);
                lastImageContainer = new ImageContainer();
                lastImageContainer.uri = i_uri;
            }
        } else {
            // this means the gallery did not return us an image
            gallery_already_called = false;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle save_bundle){
        super.onSaveInstanceState(save_bundle);
        save_bundle.putFloat(SIS_CUT_LINE_TOP,cut_line_top);
        save_bundle.putFloat(SIS_CUT_LINE_BOTTOM,cut_line_bottom);
        save_bundle.putFloat(SIS_CUT_LINE_LEFT,cut_line_left);
        save_bundle.putFloat(SIS_CUT_LINE_RIGHT,cut_line_right);
        save_bundle.putFloat(SIS_SCALE_RATIO,scale_ratio);
        save_bundle.putBoolean(SIS_IMAGE_NEEDS_TO_BE_SAVED,image_needs_to_be_saved);
        if (lastImageContainer != null){
            if (lastImageContainer.uri!=null) {
                save_bundle.putParcelable(SIS_LAST_IMAGE_URI,lastImageContainer.uri);
            }
        }
        save_bundle.putBoolean(SIS_NO_IMAGE_LOADED,no_image_loaded);
        save_bundle.putLong(SIS_JPEG_FILESIZE,jpeg_ecoded_filesize);
        save_bundle.putLong(SIS_ORIGINAL_FILESIZE,original_image_filesize);
        save_bundle.putBoolean(SIS_PIPING_ALREADY_LAUNCHED,piping_was_already_launched);
    }

    private void getSavedInstanceStateCutLine(Bundle bundle){
        cut_line_top=bundle.getFloat(SIS_CUT_LINE_TOP,0);
        cut_line_bottom=bundle.getFloat(SIS_CUT_LINE_BOTTOM, image.getHeight());
        cut_line_left=bundle.getFloat(SIS_CUT_LINE_LEFT,0);
        cut_line_right=bundle.getFloat(SIS_CUT_LINE_RIGHT,image.getWidth());
        scale_ratio=bundle.getFloat(SIS_SCALE_RATIO,getScaleRatio());
    }

    private void readPreferences(){
        pref = new ImagepipePreferences(this);
        pref.readPreferences();
        TextView scale_view = (TextView) findViewById(R.id.editText_size);
        SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
        scale_view.setText(pref.scalemax);
        seekbar_quality.setMax(pref.getQualitymaxvalue());
        seekbar_quality.setProgress(pref.quality);
    }

    private void writePreferences(){
        TextView scale_view = (TextView) findViewById(R.id.editText_size);
        SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
        pref.scalemax = scale_view.getText().toString();
        pref.quality = seekbar_quality.getProgress();
        pref.savePreferences();
    }

    public void showIntroDialog(){
        aboutDialog = new Dialog(this);
        aboutDialog.setContentView(R.layout.aboutdialog);
        aboutDialog.setTitle(getResources().getString(R.string.app_name));
        aboutDialog.setCancelable(true);
        String versioning = BuildConfig.VERSION_NAME + " (build "+BuildConfig.VERSION_CODE+")";
        TextView heading = (TextView) aboutDialog.findViewById(R.id.text_intro_headtext);
        heading.setText(getResources().getString(R.string.intro_headtext_text)+System.getProperty("line.separator")+"version "+versioning);
        ImageView iv = (ImageView) aboutDialog.findViewById(R.id.intro_headimage);
        iv.setImageResource(R.mipmap.ic_launcher);
        Button contbutton = (Button) aboutDialog.findViewById(R.id.intro_button);
        contbutton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v){
                aboutDialog.dismiss();
            }
        });
        aboutDialog.show();
    }

    public void showExifDialog(){
        exifDialog = new Dialog(this);
        exifDialog.setContentView(R.layout.exifdialog);
        exifDialog.setTitle(getResources().getString(R.string.exif_tags));
        exifDialog.setCancelable(true);
        TextView textView = (TextView) exifDialog.findViewById(R.id.exifdialog_text);
        textView.setText(getExifInfoText());
        Button contbutton = (Button) exifDialog.findViewById(R.id.exifdialog_button);
        contbutton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v){
                exifDialog.dismiss();
            }
        });
        exifDialog.show();
    }

    private void addTagLine(ExifInterface exif_data, StringBuilder stringBuilder, int min_api_level, String label, String tag){
        try{
            if (android.os.Build.VERSION.SDK_INT >= min_api_level) {
                String attribute = exif_data.getAttribute(tag);
                if (attribute != null){
                    stringBuilder.append(label + ": "+ attribute + System.lineSeparator());
                }
            }
        } catch (Exception e){
            // unknown tag in api, ignore
        }
    }

    private String getExifInfoText(){
        if (exif_data==null){
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        if (exif_data != null){
            addTagLine(exif_data,stringBuilder,11,getApplicationContext().getResources().getString(R.string.exiflabel_aperture),ExifInterface.TAG_APERTURE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_aperture),ExifInterface.TAG_APERTURE_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_maxapterture),ExifInterface.TAG_APERTURE_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_artist),ExifInterface.TAG_ARTIST);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_bps),ExifInterface.TAG_BITS_PER_SAMPLE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_brightness),ExifInterface.TAG_BRIGHTNESS_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_cfa),ExifInterface.TAG_CFA_PATTERN);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_cspace),ExifInterface.TAG_COLOR_SPACE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_compcfg),ExifInterface.TAG_COMPONENTS_CONFIGURATION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_cbpp),ExifInterface.TAG_COMPRESSED_BITS_PER_PIXEL);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_compression),ExifInterface.TAG_COMPRESSION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_contrast),ExifInterface.TAG_CONTRAST);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_copyright),ExifInterface.TAG_COPYRIGHT);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_cr),ExifInterface.TAG_CUSTOM_RENDERED);
            addTagLine(exif_data,stringBuilder,14,getApplicationContext().getResources().getString(R.string.exiflabel_datetime),ExifInterface.TAG_DATETIME);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_datetimed),ExifInterface.TAG_DATETIME_DIGITIZED);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_datetimeo),ExifInterface.TAG_DATETIME_ORIGINAL);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_dcs),ExifInterface.TAG_DEFAULT_CROP_SIZE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ds),ExifInterface.TAG_DEVICE_SETTING_DESCRIPTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_digzoom),ExifInterface.TAG_DIGITAL_ZOOM_RATIO);
            addTagLine(exif_data,stringBuilder,26,getApplicationContext().getResources().getString(R.string.exiflabel_dng),ExifInterface.TAG_DNG_VERSION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_exifv),ExifInterface.TAG_EXIF_VERSION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ebv),ExifInterface.TAG_EXPOSURE_BIAS_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_expi),ExifInterface.TAG_EXPOSURE_INDEX);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_expm),ExifInterface.TAG_EXPOSURE_MODE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_expp),ExifInterface.TAG_EXPOSURE_PROGRAM);
            addTagLine(exif_data,stringBuilder,11,getApplicationContext().getResources().getString(R.string.exiflabel_expt),ExifInterface.TAG_EXPOSURE_TIME);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fs),ExifInterface.TAG_FILE_SOURCE);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_flash),ExifInterface.TAG_FLASH);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_flashe),ExifInterface.TAG_FLASH_ENERGY);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fpv),ExifInterface.TAG_FLASHPIX_VERSION);
            addTagLine(exif_data,stringBuilder,8,getApplicationContext().getResources().getString(R.string.exiflabel_focallength),ExifInterface.TAG_FOCAL_LENGTH);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_focallength35),ExifInterface.TAG_FOCAL_LENGTH_IN_35MM_FILM);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fpru),ExifInterface.TAG_FOCAL_PLANE_RESOLUTION_UNIT);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fpx),ExifInterface.TAG_FOCAL_PLANE_X_RESOLUTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fpy),ExifInterface.TAG_FOCAL_PLANE_Y_RESOLUTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fnumber),ExifInterface.TAG_F_NUMBER);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gaincontrol),ExifInterface.TAG_GAIN_CONTROL);
            addTagLine(exif_data,stringBuilder,9,getApplicationContext().getResources().getString(R.string.exiflabel_gps_altitude),ExifInterface.TAG_GPS_ALTITUDE);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_gps_longitude),ExifInterface.TAG_GPS_LONGITUDE);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_gps_latitude),ExifInterface.TAG_GPS_LATITUDE);
            addTagLine(exif_data,stringBuilder,9,getApplicationContext().getResources().getString(R.string.exiflabel_gps_latitude_r),ExifInterface.TAG_GPS_DEST_LATITUDE_REF);
            addTagLine(exif_data,stringBuilder,9,getApplicationContext().getResources().getString(R.string.exiflabel_gps_altitude_r),ExifInterface.TAG_GPS_ALTITUDE_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_areainfo),ExifInterface.TAG_GPS_AREA_INFORMATION);
            addTagLine(exif_data,stringBuilder,8,getApplicationContext().getResources().getString(R.string.exiflabel_gps_datestamp),ExifInterface.TAG_GPS_DATESTAMP);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_bearing),ExifInterface.TAG_GPS_DEST_BEARING);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_bearingref),ExifInterface.TAG_GPS_DEST_BEARING_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_distance),ExifInterface.TAG_GPS_DEST_DISTANCE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_distanceref),ExifInterface.TAG_GPS_DEST_DISTANCE_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_destlat),ExifInterface.TAG_GPS_DEST_LATITUDE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_destlatref),ExifInterface.TAG_GPS_DEST_LATITUDE_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_destlong),ExifInterface.TAG_GPS_DEST_LONGITUDE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_destlongref),ExifInterface.TAG_GPS_DEST_LONGITUDE_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_differential),ExifInterface.TAG_GPS_DIFFERENTIAL);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_dop),ExifInterface.TAG_GPS_DOP);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_img),ExifInterface.TAG_GPS_IMG_DIRECTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_imgdir),ExifInterface.TAG_GPS_IMG_DIRECTION_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_mapdatum),ExifInterface.TAG_GPS_MAP_DATUM);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_measuremode),ExifInterface.TAG_GPS_MEASURE_MODE);
            addTagLine(exif_data,stringBuilder,8,getApplicationContext().getResources().getString(R.string.exiflabel_gps_processingmethod),ExifInterface.TAG_GPS_PROCESSING_METHOD);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_satellites),ExifInterface.TAG_GPS_SATELLITES);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_speed),ExifInterface.TAG_GPS_SPEED);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_speedref),ExifInterface.TAG_GPS_SPEED_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_status),ExifInterface.TAG_GPS_STATUS);
            addTagLine(exif_data,stringBuilder,8,getApplicationContext().getResources().getString(R.string.exiflabel_gps_timestamp),ExifInterface.TAG_GPS_TIMESTAMP);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_track),ExifInterface.TAG_GPS_TRACK);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_trackref),ExifInterface.TAG_GPS_TRACK_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_versionid),ExifInterface.TAG_GPS_VERSION_ID);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_image_description),ExifInterface.TAG_IMAGE_DESCRIPTION);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_image_length),ExifInterface.TAG_IMAGE_LENGTH);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_image_unique_id),ExifInterface.TAG_IMAGE_UNIQUE_ID);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_image_width),ExifInterface.TAG_IMAGE_WIDTH);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_interop_index),ExifInterface.TAG_INTEROPERABILITY_INDEX);
            addTagLine(exif_data,stringBuilder,11,getApplicationContext().getResources().getString(R.string.exiflabel_iso),ExifInterface.TAG_ISO);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_iso),ExifInterface.TAG_ISO_SPEED_RATINGS);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_jpegformat),ExifInterface.TAG_JPEG_INTERCHANGE_FORMAT);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_jpegformat_length),ExifInterface.TAG_JPEG_INTERCHANGE_FORMAT_LENGTH);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ls),ExifInterface.TAG_LIGHT_SOURCE);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_make),ExifInterface.TAG_MAKE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_makernote),ExifInterface.TAG_MAKER_NOTE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_max_aperture_value),ExifInterface.TAG_MAX_APERTURE_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_metering_mode),ExifInterface.TAG_METERING_MODE);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_model),ExifInterface.TAG_MODEL);
            addTagLine(exif_data,stringBuilder,26,getApplicationContext().getResources().getString(R.string.exiflabel_new_subfile_type),ExifInterface.TAG_NEW_SUBFILE_TYPE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_oecf),ExifInterface.TAG_OECF);
            // api 30 not implemented yet
            //addTagLine(exif_data,stringBuilder,30,getApplicationContext().getResources().getString(R.string.exiflabel_offset_time),ExifInterface.TAG_OFFSET_TIME);
            //addTagLine(exif_data,stringBuilder,30,getApplicationContext().getResources().getString(R.string.exiflabel_offset_time_digitized),ExifInterface.TAG_OFFSET_TIME_DIGITIZED);
            // addTagLine(exif_data,stringBuilder,30,getApplicationContext().getResources().getString(R.string.exiflabel_offset_time_original),ExifInterface.TAG_OFFSET_TIME_ORIGINAL);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_orientation),ExifInterface.TAG_ORIENTATION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_pi),ExifInterface.TAG_PHOTOMETRIC_INTERPRETATION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_pixel_x_dim),ExifInterface.TAG_PIXEL_X_DIMENSION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_pixel_y_dim),ExifInterface.TAG_PIXEL_Y_DIMENSION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_planarconf),ExifInterface.TAG_PLANAR_CONFIGURATION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_prim_chromat),ExifInterface.TAG_PRIMARY_CHROMATICITIES);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_bw),ExifInterface.TAG_REFERENCE_BLACK_WHITE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_related_soundfile),ExifInterface.TAG_RELATED_SOUND_FILE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_resolution_unit),ExifInterface.TAG_RESOLUTION_UNIT);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_rps),ExifInterface.TAG_ROWS_PER_STRIP);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_samples_per_pixel),ExifInterface.TAG_SAMPLES_PER_PIXEL);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_saturation),ExifInterface.TAG_SATURATION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_scene_capture_type),ExifInterface.TAG_SCENE_CAPTURE_TYPE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_scene_type),ExifInterface.TAG_SCENE_TYPE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_sensing_method),ExifInterface.TAG_SENSING_METHOD);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_sharpness),ExifInterface.TAG_SHARPNESS);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_shutterspeedvalue),ExifInterface.TAG_SHUTTER_SPEED_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_software),ExifInterface.TAG_SOFTWARE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_spatial_frequency_response),ExifInterface.TAG_SPATIAL_FREQUENCY_RESPONSE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_spectral_sensitivity),ExifInterface.TAG_SPECTRAL_SENSITIVITY);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_strip_byte_counts),ExifInterface.TAG_STRIP_BYTE_COUNTS);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_strip_offsets),ExifInterface.TAG_STRIP_OFFSETS);
            addTagLine(exif_data,stringBuilder,26,getApplicationContext().getResources().getString(R.string.exiflabel_subfile_type),ExifInterface.TAG_SUBFILE_TYPE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subject_area),ExifInterface.TAG_SUBJECT_AREA);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subject_dist),ExifInterface.TAG_SUBJECT_DISTANCE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subject_distance_range),ExifInterface.TAG_SUBJECT_DISTANCE_RANGE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subject_loc),ExifInterface.TAG_SUBJECT_LOCATION);
            addTagLine(exif_data,stringBuilder,23,getApplicationContext().getResources().getString(R.string.exiflabel_subsec_time),ExifInterface.TAG_SUBSEC_TIME);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subsec_time_dig),ExifInterface.TAG_SUBSEC_TIME_DIGITIZED);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subsec_time_orig),ExifInterface.TAG_SUBSEC_TIME_ORIGINAL);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_transfer),ExifInterface.TAG_TRANSFER_FUNCTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_user_comment),ExifInterface.TAG_USER_COMMENT);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_white_balance),ExifInterface.TAG_WHITE_BALANCE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_white_point),ExifInterface.TAG_WHITE_POINT);
            // addTagLine(exif_data,stringBuilder,29,getApplicationContext().getResources().getString(R.string.exiflabel_xmp),ExifInterface.TAG_XMP);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_x_resolution),ExifInterface.TAG_X_RESOLUTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ycbcr_co),ExifInterface.TAG_Y_CB_CR_COEFFICIENTS);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ycbcr_pos),ExifInterface.TAG_Y_CB_CR_POSITIONING);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ycbcr_sub),ExifInterface.TAG_Y_CB_CR_SUB_SAMPLING);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_y_resolution),ExifInterface.TAG_Y_RESOLUTION);
        }
        return stringBuilder.toString();
    }


}
